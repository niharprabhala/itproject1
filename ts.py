# Rumeet Goradia - rug5, 177008120
# Nihar Prabhala - np642, 179005552

import socket as mysoc
import sys
import threading


def server():
    try:
        ss = mysoc.socket(mysoc.AF_INET, mysoc.SOCK_STREAM)
        # print("[S]: Server socket created")
    except mysoc.error as err:
        print('{}} \n'.format("socket open error ", err))
    tsListenPort = int(sys.argv[1])
    server_binding = ('', tsListenPort)
    ss.bind(server_binding)
    ss.listen(1)
    host = mysoc.gethostname()
   # print("[S]: Server host name is: ", host)
    localhost_ip = (mysoc.gethostbyname(host))
   # print("[S]: Server IP address is  ", localhost_ip)

    dns_table_ts = {}
    ts_file = open('./PROJI-DNSTS.txt', "r")

    # create dictionary from file
    for line in ts_file:
        line_split = line.split()
        dns_table_ts[line_split[0]] = (line_split[1], line_split[2])

    # list of keys in dictionary converted to lowercase
    # lower_keys = dns_table_ts.keys()
    # lower_keys = [x.lower() for x in lower_keys]

    # lower-case dictionary, same tuple value
    lower_dict = {}
    for x in dns_table_ts:
        lower_dict[x.lower()] = (x, dns_table_ts.get(x)
                                 [0], dns_table_ts.get(x)[1])
    hostNameMessage = ""
    while(hostNameMessage != "$END$"):
        csockid, addr = ss.accept()
      #  print("[S]: Got a connection request from a client at", addr)

        # search thru lower-case dictionary, using input Hostname
        retMessage = ""
        # get host name from client
        hostNameMessage = csockid.recv(250).decode('utf-8')
        if (hostNameMessage != "$END$"):
            if hostNameMessage.lower() in lower_dict:
                value = lower_dict.get(hostNameMessage.lower())
                hostName = value[0]
                ip_add = value[1]
                flag = value[2]
                retMessage = hostName + " " + ip_add + " " + flag
            else:
                retMessage = hostNameMessage + " - Error:HOST NOT FOUND"

                # if the hostname is found, case-insensitive, use
                # if hostNameMessage.lower() in lower_keys:
                # 	value = dns_table_ts.get(hostNameMessage)
                # 	ip_add = value[0]
                # 	flag = value[1]
                # 	retMessage = hostNameMessage + " " + ip_add + " " + flag
                # else:
                # 	retMessage = hostNameMessage + "- Error:HOST NOT FOUND"
            csockid.send(retMessage.encode('utf-8'))
        ts_file.close()


if (len(sys.argv) != 2):
    sys.exit("Improper number of arguments. Please try again with 1 argument.")

t1 = threading.Thread(name='server', target=server)
t1.start()
