# Rumeet Goradia - rug5, 177008120
# Nihar Prabhala - np642, 179005552

import socket as mysoc
import sys
import threading

outputStr = ""


def clientrs():
    try:
        cs = mysoc.socket(mysoc.AF_INET, mysoc.SOCK_STREAM)
    except mysoc.error as err:
        print('{} \n'.format("socket open error ", err))

    # Define the port on which you want to connect to the server
    rsPort = int(sys.argv[2])
    rsIP = mysoc.gethostbyname(sys.argv[1])

    # connect to the server on local machine
    rsServerBinding = (rsIP, rsPort)
    try:
        cs.connect(rsServerBinding)
    except:
        print("Invalid RS port number or hostname.")
        exit()

    # open file with hostnames to be queried
    inputFile = open('./PROJI-HNS.txt', "r")

    # send number of lines
    lineCount = len(inputFile.readlines())
    cs.send(str(lineCount).encode('utf-8'))

    # send hostnames to RS
    inputFile.seek(0)
    tsHost = ""

    global outputStr

    for queriedHostname in inputFile:
        cs.send(queriedHostname.encode('utf-8'))
        queriedHostname = queriedHostname[0:len(queriedHostname) - 1]

        # receive and use RS's response
        rsResponse = cs.recv(250).decode('utf-8')
        rsResponseSplit = rsResponse.split(" ")

        if (rsResponseSplit[2].rstrip() == "NS"):
            tsHost = rsResponseSplit[0].rstrip()
            tsThread = threading.Thread(
                name='clientts', target=clientts, args=(tsHost, queriedHostname))  # ts thread
            tsThread.start()
            tsThread.join()
        elif (rsResponseSplit[2].rstrip() == "A"):
            outputStr = outputStr + rsResponse + "\n"
        else:
            print("Invalid response from the RS server.")

# send fin message to ts server
    tsThread = threading.Thread(
        name='clientts', target=clientts, args=(tsHost, "$END$"))  # ts thread
    tsThread.start()
    tsThread.join()

# write output
    outputFile = open("./RESOLVED.txt", "w")
    output = outputStr.rstrip()
    outputFile.write(output)

# closing procedures
    outputFile.close()
    inputFile.close()
    cs.close()
    exit()


def clientts(tsHostname, queriedHostname):
    try:
        cs = mysoc.socket(mysoc.AF_INET, mysoc.SOCK_STREAM)
    except mysoc.error as err:
        print('{} \n'.format("socket open error ", err))

# Define the port on which you want to connect to the server
    tsPort = int(sys.argv[3])
    tsIP = mysoc.gethostbyname(tsHostname)

# connect to the server on local machine
    try:
        server_binding = (tsIP, tsPort)
        cs.connect(server_binding)
    except mysoc.error as error:
        print(error)
        exit()

# send queried hostname to TS server
    cs.send(queriedHostname.encode('utf-8'))

# output message from TS to output file
    tsResponse = cs.recv(250).decode('utf-8')
    global outputStr
    outputStr += tsResponse + "\n"

# closing procedures
    cs.close()
    exit()


if (len(sys.argv) != 4):
    sys.exit("Improper number of arguments. Please try again with 3 arguments.")

rsThread = threading.Thread(name='clientrs', target=clientrs)  # rs thread
rsThread.start()

exit()
