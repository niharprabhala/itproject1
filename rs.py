# Rumeet Goradia - rug5, 177008120
# Nihar Prabhala - np642, 179005552

import socket as mysoc
import sys
import threading


def server():
    try:
        ss = mysoc.socket(mysoc.AF_INET, mysoc.SOCK_STREAM)
        # print("[S]: Server socket created")
    except mysoc.error as err:
        print('{} \n'.format("socket open error ", err))
    rsListenPort = int(sys.argv[1])
    server_binding = ('', rsListenPort)
    ss.bind(server_binding)
    ss.listen(1)
    host = mysoc.gethostname()
   # print("[S]: Server host name is: ", host)
    localhost_ip = (mysoc.gethostbyname(host))
   # print("[S]: Server IP address is  ", localhost_ip)
    csockid, addr = ss.accept()
   # print("[S]: Got a connection request from a client at", addr)

    # creating table data structure
    dns_table_rs = {}
    rs_file = open('./PROJI-DNSRS.txt', "r")

    TSHost = ""

    for line in rs_file:
        line_split = line.split()
        if (line_split[2] == "NS"):
            TSHost = line[0:len(line)-1]
        else:
            dns_table_rs[line_split[0]] = (line_split[1], line_split[2])

        # create lower-case dictionary, same tuple value
    lower_dict = {}
    for x in dns_table_rs:
        lower_dict[x.lower()] = (x, dns_table_rs.get(x)
                                 [0], dns_table_rs.get(x)[1])

    # number of strings received from client
    numMessages = int(csockid.recv(6).decode('utf-8'))

    # find local host name with NS flag
    # for i in dns_table_rs:
    #     tup = dns_table_rs.get(i)
    #     if (tup[1] == "NS"):
    #         TSHost = i
    retMessage = ""
    for x in range(0, numMessages):
        hostNameMessage = csockid.recv(250).decode('utf-8').lower()
        hostNameMessage = hostNameMessage[0:len(hostNameMessage) - 1]
        # print(hostNameMessage)
        # for x in lower_dict:
        #     print(x)
        if hostNameMessage in lower_dict:
            value = lower_dict.get(hostNameMessage)
            hostName = value[0]
            ip_add = value[1]
            flag = value[2]
            retMessage = hostName + " " + ip_add + " " + flag
        else:
            # send TShostname + NS flag
            retMessage = TSHost
    #    print(retMessage)
        csockid.send(retMessage.encode('utf-8'))
    rs_file.close()


if (len(sys.argv) != 2):
    sys.exit("Improper number of arguments. Please try again with 1 argument.")

t1 = threading.Thread(name='server', target=server)
t1.start()
