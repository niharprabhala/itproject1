Rumeet Goradia - rug5, 177008120  
Nihar Prabhala - np642, 179005552

1. Briefly discuss how you implemented your recursive client functionality

   The client first connects to the RS server via a thread. It first sends the number of hostnames to be queried to the RS server, so the RS server knows when to close. It then sends the queried hostnames to the RS server one by one. For each queried hostname, the client checks the response from the RS server; the possible cases are as follows:

   - If the response contains the 'A' flag, then the client concatenates the response to an output string.
   - If the response contains the 'NS' flag, then the client starts a thread to connect to the TS server. It then sends the queried hostname to the TS server, and then concatenates the response to the output string.
   - If the response contains neither flag, then the client ignores the response.

   The client then sends a special ending string to the TS server so that the TS server knows it can close. Then, the output string in the client is written to the output file.

2. Are there known issues or functions that aren't working currently in your attached code? If so, explain.

   After extensive testing with all of the edge cases that we could think of, we did not determine that there are any issues with the functionality of our code.

3. What problems did you face developing code for this project?

   One of the problems we ran into when developing code for this project was how to be able to make case-insensitive look-ups in the table. We used a dictionary to model the DNS table to maintain the entries from the files. The dictionary had the hostname as the key and a tuple of the IP address and flag as the value. It was difficult to compare, case-insensitively, the query to the table hostnames without changing the state of the dictionary. Initially, to get around this problem, we made another dictionary that had the lowercase hostnames as keys mapped to the same tuple values. However, this did not solve the problem as we needed the hostname that was stored in the actual table, not the one that was being queried or lowercase. So instead, we modified our dictionary to contain the lowercase value of the hostnames from the file as the key and had a tuple containing the unmodified hostname, IP address, and flag as the corresponding value. We then compared the queried hostname (converted to lowercase) and were able to retrieve all the correct values that we needed from the tuple.

4. What did you learn by working on this project?

   By working on this project, we learned that a single thread was not able to handle multiple socket connections between a client and server. Another thing we learned was how to manipulate the values we store in a data structure (i.e dictionary) in order to optimize the rest of the queries that were happening in the code.
